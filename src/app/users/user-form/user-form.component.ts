import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../shared/types';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';

const fields = {
  commonInfo: {
    name: {
      validators: ['required']
    },
    phone: {
      validators: ['required']
    },
    email: {
      validators: ['required', 'email']
    },
    website: {
      validators: ['required']
    }
  },

  company: {
    name: {
      validators: ['required']
    },
    catchPhrase: {
      validators: []
    },
    bs: []
  },

  address: {
    city: {
      validators: ['required']
    },
    street: {
      validators: ['required']
    }
  }
};


@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})

export class UserFormComponent implements OnInit {

  user: User;

  form: FormGroup;

  newData = {};


  get bsFormArray(): FormArray {
    return this.form.get('company').get('bs') as FormArray;
  }

  fieldNameMap = {
    commonInfo: [
      {
        label: 'Name',
        field: 'name'
      },
      {
        label: 'Phone',
        field: 'phone'
      },
      {
        label: 'Email',
        field: 'email'
      },
      {
        label: 'Website',
        field: 'website'
      },
    ],
    company: [
      {
        label: 'Name',
        field: 'name'
      },
      {
        label: 'Catch Phrase',
        field: 'catchPhrase'
      },
    ],
    address: [
      {
        label: 'City',
        field: 'city'
      },
      {
        label: 'Street',
        field: 'street'
      }
    ]
  };


  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.form = new FormGroup({});

    Object.keys(fields).forEach(groupKey => {

      this.form.addControl(groupKey, new FormGroup({}));

      const group = this.form.get(groupKey) as FormGroup;

      Object.keys(fields[groupKey]).forEach(fieldKey => {
        if (Array.isArray(fields[groupKey][fieldKey])) {
          group.addControl(fieldKey, new FormArray([]));
          return;
        }

        const validators = [];
        if (fields[groupKey][fieldKey].validators && Array.isArray(fields[groupKey][fieldKey].validators)) {
          fields[groupKey][fieldKey].validators.forEach(validatorName => {
            switch (validatorName) {
              case 'required':
                validators.push(Validators.required);
                break;
              case 'email':
                validators.push(Validators.email);
                break;
            }
          });
        }

        const control = new FormControl('', validators);
        group.addControl(fieldKey, control);
      });
    });

    console.log('FORMGROUP: ', this.form);
    this.route.data
      .subscribe(({userDetail}) => {
        this.user = userDetail;
        this.form.get('commonInfo').patchValue(this.user);
        this.form.get('company').patchValue({...this.user.company, bs: []});
        this.user.company.bs.split(' ').forEach(bsE1 => {
          this.bsFormArray.push(new FormControl(bsE1));
        });
        this.form.get('address').patchValue({...this.user.address});
        console.log('userDetail: ', userDetail);
        console.log('user: ', this.user);
      });
  }

  addBS(): any {
    this.bsFormArray.push(new FormControl(''));
  }

  deleteBS(item): any {
    this.bsFormArray.removeAt(item);
    console.log(this.bsFormArray.value.join(' '));
  }

  saveData(): void {
    this.form.value.company.bs = this.bsFormArray.value.join(' ');
    this.newData = this.form.value;
    const jsonData = JSON.stringify(this.newData).replace(/"([^"]+)":/g, '$1:');

    console.log('new data:', jsonData);
  }

}
