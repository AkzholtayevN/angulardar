import { Component, OnInit } from '@angular/core';
import {NavItem} from '../shared/types';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  navItemsFromApp: NavItem [] = [
    {
      title: 'Users',
      enabled: true,
      url: '/users'
    },
    {
      title: 'Videos',
      enabled: true,
      url: '/videos'
    },
    {
      title: 'Rooms',
      enabled: true,
      url: ''
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
